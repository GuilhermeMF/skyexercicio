package com.guilhermemfresca.skyexercicio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guilhermemfresca.skyexercicio.R;
import com.guilhermemfresca.skyexercicio.json.Movie;

import java.util.ArrayList;

public class MovieAdapter extends BaseAdapter {

    private ArrayList<Movie> movies;
    private Context context;
    private LayoutInflater mInflater;
    private ViewHolder holder;

    public MovieAdapter(Context context, ArrayList<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }


    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView txt_movie;
        ImageView img_movie;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie item = movies.get(position);

        if (convertView == null) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.include_list_filmes, parent, false);
            holder = new ViewHolder();

            holder.txt_movie = (TextView) convertView.findViewById(R.id.txt_a);
            holder.img_movie = (ImageView) convertView.findViewById(R.id.img_a);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        Glide.with(context)
                .load(item.getCover_url())
                .into(holder.img_movie);
        holder.txt_movie.setText(item.getTitle());

        return convertView;
    }
}
