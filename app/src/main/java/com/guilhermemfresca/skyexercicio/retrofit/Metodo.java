package com.guilhermemfresca.skyexercicio.retrofit;

import com.guilhermemfresca.skyexercicio.json.Movie;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Metodo {

    @GET("Movies/")
    Call<ArrayList<Movie>> getMovie();
}
