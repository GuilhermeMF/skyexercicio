package com.guilhermemfresca.skyexercicio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.guilhermemfresca.skyexercicio.adapter.MovieAdapter;
import com.guilhermemfresca.skyexercicio.json.Movie;
import com.guilhermemfresca.skyexercicio.retrofit.Metodo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    GridView grid_movie;
    MovieAdapter adapter;
    ArrayList<Movie> moviesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        try {
            __ListaMovie();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init(){
        grid_movie = (GridView) findViewById(R.id.gird_list);

    }

    private void __ListaMovie() {

        Metodo retrofit = new Retrofit.Builder()
                .baseUrl("https://sky-exercise.herokuapp.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Metodo.class);

        Call<ArrayList<Movie>> callOs = retrofit.getMovie();
        callOs.enqueue(new Callback<ArrayList<Movie>>() {
            @Override
            public void onResponse(Call<ArrayList<Movie>> call, Response<ArrayList<Movie>> response) {

                moviesList = response.body();
                System.out.println(moviesList.get(1).getId().toString());
                ListarMovie();
            }

            @Override
            public void onFailure(Call<ArrayList<Movie>> call, Throwable t) {

            }

        });
    }

    private void ListarMovie(){
        adapter = new MovieAdapter(getApplicationContext(), moviesList);
        grid_movie.setAdapter(adapter);
    }
}
